# Pi approximations

A simple SPA demonstrating some pi approximation algorithms.

## Installation

```
npm install
node_modules/.bin/bower install
node_modules/.bin/grunt dist
```

If you prefer to build a development version of the project, run the `dev` task instead of `dist`. The built project will be in the `app` directory.

## Running

Once installed and built, `cd` to the `dist` directory (or `app`, if you have built the dev version) and start a dev webserver, e.g.

```
php -S localhost:8080
```

or

```
httpster -p 8080
```

Then, open your web browser and navigate to `http://localhost:8080`.

## Tests

To run the tests, run Karma directly:

```
node_modules/.bin/karma start
```
