#!/bin/bash
echo Branch is $BRANCH.
if [ "$BRANCH" == "master" ]; then
	echo Deploying...
	node_modules/.bin/grunt dist
	tar -zcf pi-approximations.tar.gz dist/
	scp -i ~/.ssh/id_rsa pi-approximations.tar.gz ci@umbraprojekt.pl:/home/ci/
	rm -f pi-approximations.tar.gz
	echo Deployment finished.
fi

