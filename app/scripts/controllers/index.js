/* Pi Approximations
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

angular.module("pi").controller("IndexCtrl", [
	"$scope",
	"$http",
	"$injector",
	function(
		$scope,
		$http,
		$injector // uncool to use it, but grants IoC
	) {
		// available algorithm precisions
		$scope.precisions = [];
		for (var i = 1; i <= 15; ++i) {
			$scope.precisions.push(i);
		}

		// chosen precision
		$scope.precision = 5;

		// chosen algorithm and corresponding header
		$scope.algorithm = "";
		$scope.header = "";

		// available algorithms
		$scope.algorithms = [];

		// visualisation
		$scope.visualisation = "table";

		$http({
			method: "GET",
			url: "/data/algorithms.json"
		}).success(function(data) {
			$scope.algorithms = data;
			$scope.algorithm = $scope.algorithms[0].service;
			$scope.header = $scope.algorithms[0].label;
		});

		// the calculated series will go here
		$scope.series = [];

		// calculate me baby one more time!
		$scope.calculate = function()
		{
			// select the header for the visualisations
			angular.forEach($scope.algorithms, function(item) {
				if (item.service === $scope.algorithm) {
					$scope.header = item.label;
				}
			});

			// which visualisation to show
			$scope.showTable = $scope.visualisation === "table";
			$scope.showChart = $scope.visualisation === "chart";

			// calculate the series
			$scope.series = $injector.get($scope.algorithm).getSteps($scope.precision);
		};
	}
]);
