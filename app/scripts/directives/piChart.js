/* Pi Approximations
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

angular.module("pi").directive("piChart", [
	function() {
		return {
			restrict: "A",
			scope: {
				series: "="
			},
			link: function(scope, element, attr) {
				var buildChart = function()
				{
					var seriesData = [];
					angular.forEach(scope.series, function(value, key) {
						seriesData.push([key, value]);
					});

					new Highcharts.Chart({
						chart: {
							renderTo: element[0],
							type: "line"
						},
						title: {
							text: attr.header || ""
						},
						xAxis: {
							title: {
								text: "k value"
							},
							allowDecimals: false
						},
						yAxis: {
							title: {
								text: "Approximate value"
							},
							plotLines: [
								{
									color: "#ff0000",
									width: 1,
									value: Math.PI,
									dashStyle: "ShortDot",
									label: {
										text: "actual pi value"
									}
								}
							]
						},
						series: [
							{
								name: "Approximate value",
								data: seriesData
							}
						]
					});
				};

				scope.$watch("series", buildChart);
			}
		};
	}
]);
