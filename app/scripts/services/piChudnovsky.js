/* Pi Approximations
 * Copyright (c) 2014 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

angular.module("pi").factory("piChudnovsky", [
	"seriesInterface",
	function(
		seriesInterface
	) {
		var series = new seriesInterface();

		series.getStep = function(k)
		{
			k = parseInt(k, 10); 
			var result = (Math.pow(-1, k) * Math.factorial(6 * k) * (163 * 3344418 * k + 13591409)) / (Math.factorial(3 * k) * Math.pow(Math.factorial(k), 3) * Math.pow(640320, (3 * k + 3 / 2)));

			// multiply by 12 as the result is 12 * sum of steps
			return 12 * result;
		};

		series.getApproximationAtStep = function(k)
		{
			k = parseInt(k, 10);
			var i = 0,
				output = 0;

			for (; i <= k; ++i) {
				output += this.getStep(i);
			}

			// the algorithm calculates 1/x, so return inverse to get a pi approximation
			return 1 / (output);
		};

		return series;
	}
]);
