describe("Directive: piTable", function() {
	beforeEach(module("pi"));

	var element;
	beforeEach(inject(function($compile, $rootScope) {
		$rootScope.items = [];
		$rootScope.header = "";
		element = angular.element('<table pi-table series="items" header="{{ header }}"></table>');
		$compile(element)($rootScope);
		$rootScope.$digest();
	}));

	it("should contain no items if there's no data and hide the header if it's empty", function() {
		expect(element.find("tr").eq(0).hasClass("ng-hide")).toBeTruthy();
		expect(element.find("td").length).toBe(0);
	});

	it("should display the header and items correctly", inject(function($rootScope) {
		$rootScope.header = "Lumberjack's song";
		$rootScope.items = ["I'm a lumberjack", "And I'm OK", "I sleep all night", "And I work all day"];
		$rootScope.$digest();

		expect(element.find("tr").eq(0).find("th").eq(0).text()).toBe("Lumberjack's song");

		// no tr with index 1 - this one is empty. Possibly a quirk in Angular.js
		expect(element.find("tr").eq(2).find("td").eq(0).text()).toBe("k = 0");
		expect(element.find("tr").eq(2).find("td").eq(1).text()).toBe("I'm a lumberjack");
		expect(element.find("tr").eq(3).find("td").eq(0).text()).toBe("k = 1");
		expect(element.find("tr").eq(3).find("td").eq(1).text()).toBe("And I'm OK");
		expect(element.find("tr").eq(4).find("td").eq(0).text()).toBe("k = 2");
		expect(element.find("tr").eq(4).find("td").eq(1).text()).toBe("I sleep all night");
		expect(element.find("tr").eq(5).find("td").eq(0).text()).toBe("k = 3");
		expect(element.find("tr").eq(5).find("td").eq(1).text()).toBe("And I work all day");
	}));
});
