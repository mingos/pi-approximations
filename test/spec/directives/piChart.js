describe("Directive: piChart", function() {
	beforeEach(module("pi"));

	var element;
	beforeEach(inject(function($compile, $rootScope) {
		$rootScope.items = [];
		$rootScope.header = "";
		element = angular.element('<div pi-chart series="items" header="{{ header }}"></div>');
		$compile(element)($rootScope);
		$rootScope.$digest();
	}));

	it("should display a chart", inject(function($rootScope) {
		$rootScope.header = "Lumberjack's song";
		$rootScope.items = [1, 2, 3, 4, 5];
		$rootScope.$digest();

		expect(element.find("svg").length).toBe(1);

		// we trust that the chart itself is OK; probably tested by the Highcharts team :)
	}));
});
