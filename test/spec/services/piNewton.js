describe("Service: piNewton", function() {
	beforeEach(module("pi"));

	var piNewton;
	beforeEach(inject(function(_piNewton_) {
		piNewton = _piNewton_;
	}));

	var approx = function(actual, expected, precision) {
		precision = parseInt(precision || 10, 5);
		return Math.floor(actual * Math.pow(10, precision)) === Math.floor(expected * Math.pow(10, precision));
	};

	it("should calculate the steps correctly", function() {
		expect(approx(piNewton.getStep(0), 1)).toBeTruthy();
		expect(approx(piNewton.getStep(1), 0.333333333333333)).toBeTruthy();
		expect(approx(piNewton.getStep(2), 0.133333333333333)).toBeTruthy();
		expect(approx(piNewton.getStep(3), 0.057142857142857)).toBeTruthy();
		expect(approx(piNewton.getStep(4), 0.025396825396825)).toBeTruthy();
	});

	it("should calculate the pi approximation correctly at each step", function() {
		expect(approx(piNewton.getApproximationAtStep(0), 2)).toBeTruthy();
		expect(approx(piNewton.getApproximationAtStep(1), 2.666666666666666)).toBeTruthy();
		expect(approx(piNewton.getApproximationAtStep(2), 2.933333333333333)).toBeTruthy();
		expect(approx(piNewton.getApproximationAtStep(3), 3.047619047619047)).toBeTruthy();
		expect(approx(piNewton.getApproximationAtStep(4), 3.098412698412698)).toBeTruthy();
	});

	it("should return the correct steps array", function() {
		var steps = piNewton.getSteps(5);
		expect(approx(steps[0], 2)).toBeTruthy();
		expect(approx(steps[1], 2.666666666666666)).toBeTruthy();
		expect(approx(steps[2], 2.933333333333333)).toBeTruthy();
		expect(approx(steps[3], 3.047619047619047)).toBeTruthy();
		expect(approx(steps[4], 3.098412698412698)).toBeTruthy();
	});
});