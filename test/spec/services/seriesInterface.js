describe("Service: seriesInterface", function() {
	beforeEach(module("pi"));

	var seriesInterface;
	beforeEach(inject(function(_seriesInterface_) {
		seriesInterface = new _seriesInterface_();
	}));

	it("should expose the getSteps function", function() {
		expect(seriesInterface.getSteps instanceof Function).toBeTruthy();
	});

	it("should expose the getStep function", function() {
		expect(seriesInterface.getStep instanceof Function).toBeTruthy();
	});

	it("should expose the getApproximationAtStep function", function() {
		expect(seriesInterface.getApproximationAtStep instanceof Function).toBeTruthy();
	});

	it("should return zeroes by default", function() {
		expect(seriesInterface.getStep(0)).toBe(0);
		expect(seriesInterface.getStep(1)).toBe(0);
		expect(seriesInterface.getStep(2)).toBe(0);
		expect(seriesInterface.getSteps(3)).toEqual([0, 0, 0]);
	});
});