describe("Service: piMadhava", function() {
	beforeEach(module("pi"));

	var piMadhava;
	beforeEach(inject(function(_piMadhava_) {
		piMadhava = _piMadhava_;
	}));

	var approx = function(actual, expected, precision) {
		precision = parseInt(precision || 10, 10);
		return Math.floor(actual * Math.pow(10, precision)) === Math.floor(expected * Math.pow(10, precision));
	};

	it("should calculate the steps correctly", function() {
		expect(approx(piMadhava.getStep(0),  3.4641016151377544)).toBeTruthy();
		expect(approx(piMadhava.getStep(1), -0.3849001794597504)).toBeTruthy();
		expect(approx(piMadhava.getStep(2),  0.0769800358919500)).toBeTruthy();
		expect(approx(piMadhava.getStep(3), -0.0183285799742738)).toBeTruthy();
		expect(approx(piMadhava.getStep(4),  0.0047518540674043)).toBeTruthy();
	});

	it("should calculate the pi approximation correctly at each step", function() {
		expect(approx(piMadhava.getApproximationAtStep(0), 3.46410161513775)).toBeTruthy();
		expect(approx(piMadhava.getApproximationAtStep(1), 3.07920143567800)).toBeTruthy();
		expect(approx(piMadhava.getApproximationAtStep(2), 3.15618147156995)).toBeTruthy();
		expect(approx(piMadhava.getApproximationAtStep(3), 3.13785289159568)).toBeTruthy();
		expect(approx(piMadhava.getApproximationAtStep(4), 3.14260474566308)).toBeTruthy();
	});

	it("should return the correct steps array", function() {
		var steps = piMadhava.getSteps(5);
		expect(approx(steps[0], 3.46410161513775)).toBeTruthy();
		expect(approx(steps[1], 3.07920143567800)).toBeTruthy();
		expect(approx(steps[2], 3.15618147156995)).toBeTruthy();
		expect(approx(steps[3], 3.13785289159568)).toBeTruthy();
		expect(approx(steps[4], 3.14260474566308)).toBeTruthy();
	});
});