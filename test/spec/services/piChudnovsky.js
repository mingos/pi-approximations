describe("Service: piChudnovsky", function() {
	beforeEach(module("pi"));

	var piChudnovsky;
	beforeEach(inject(function(_piChudnovsky_) {
		piChudnovsky = _piChudnovsky_;
	}));

	var approx = function(actual, expected, precision) {
		// since Chudnovsky's algorithm is so precise, use a big default precision of 15 decimal places
		precision = parseInt(precision || 10, 15);
		return Math.floor(actual * Math.pow(10, precision)) === Math.floor(expected * Math.pow(10, precision));
	};

	it("should calculate the steps correctly", function() {
		expect(approx(piChudnovsky.getStep(0),  0.318309886183796)).toBeTruthy();
		expect(approx(piChudnovsky.getStep(1), -5.981069938657074e-15)).toBeTruthy();
		expect(approx(piChudnovsky.getStep(2),  3.119150391121248e-29)).toBeTruthy();
		expect(approx(piChudnovsky.getStep(3), -1.743251491765130e-43)).toBeTruthy();
		expect(approx(piChudnovsky.getStep(4),  1.013497127781742e-57)).toBeTruthy();
	});

	it("should calculate the pi approximation correctly at each step", function() {
		expect(approx(piChudnovsky.getApproximationAtStep(0), 3.1415926535897345)).toBeTruthy();
		expect(approx(piChudnovsky.getApproximationAtStep(1), 3.1415926535897936)).toBeTruthy();
		expect(approx(piChudnovsky.getApproximationAtStep(2), 3.1415926535897936)).toBeTruthy();
		expect(approx(piChudnovsky.getApproximationAtStep(3), 3.1415926535897936)).toBeTruthy();
		expect(approx(piChudnovsky.getApproximationAtStep(4), 3.1415926535897936)).toBeTruthy();
	});

	it("should return the correct steps array", function() {
		var steps = piChudnovsky.getSteps(5);
		expect(approx(steps[0], 3.1415926535897345)).toBeTruthy();
		expect(approx(steps[1], 3.1415926535897936)).toBeTruthy();
		expect(approx(steps[2], 3.1415926535897936)).toBeTruthy();
		expect(approx(steps[3], 3.1415926535897936)).toBeTruthy();
		expect(approx(steps[4], 3.1415926535897936)).toBeTruthy();
	});
});