describe("Util: factorial", function() {
	it("should correctly calculate the factorials", function() {
		var factorials = [
			1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800
		];
		for (var i in factorials) {
			expect(Math.factorial(i)).toBe(factorials[i]);
		}
	});
});
