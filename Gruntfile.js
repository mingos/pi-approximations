module.exports = function(grunt) {
	grunt.initConfig({
		concat: {
			dev: {
				files: {
					"app/js/script.js": [
						"app/components/jquery/dist/jquery.js",
						"app/components/highcharts/highcharts.src.js",
						"app/components/angular/angular.js",
						"app/scripts/**/*.js"
					]
				}
			},
		},
		less: {
			dev: {
				options: {
					cleancss: true
				},
				files: {
					"app/css/style.css": [
						"app/less/style.less"
					]
				}
			}
		},
		uglify: {
			dist: {
				options: {
					mangle: true,
					compress: {
						drop_console: true
					}
				},
				files: {
					"dist/js/script.js": [
						"app/js/script.js"
					]
				}
			}
		},
		copy: {
			dist: {
				files: [
					{ expand: true, cwd: "app/css/", src: ["style.css"], dest: "dist/css/" },
					{ expand: true, cwd: "app/", src: ["index.html"], dest: "dist/" },
					{ expand: true, cwd: "app/data/", src: ["**/*"], dest: "dist/data/" }
				]
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks('grunt-contrib-copy');

	// Custom tasks
	grunt.registerTask("default", []);

	grunt.registerTask("dev", [
		"concat:dev",
		"less:dev"
	]);
	grunt.registerTask("dist", [
		"dev",
		"uglify:dist",
		"copy:dist"
	]);
};
